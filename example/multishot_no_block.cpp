#define CO_NET_NO_WORKER_MODE
#define CO_NET_SQPOLL

#include "co_net/co_net.hpp"

using namespace net;
using namespace net::io;
using namespace net::tcp;
using namespace net::async;

Task<> echo(TcpConnection conn) {
    for (;;) {
        auto nread = co_await conn.ring_buf_receive();
        if (!nread) {
            co_return;
        }
        auto msg = conn.move_out_received();
        auto nwrite = co_await conn.write_all(msg);
    }
}

Task<> server() {
    auto listener = co_await TcpListener::listen_on(ip::make_addr_v4("localhost", 20589));
    for (;;) {
        auto conn_list = co_await listener.multishot_accept_with_no_dispatch();
        for (auto& conn : conn_list) { net::context::co_spawn(&echo, std::move(conn)); }
        co_await PendingAwaiter{};
    }
}

int main() {
    return net::context::block_on(&server);
}