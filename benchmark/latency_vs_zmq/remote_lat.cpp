#define CO_NET_NO_WORKER_MODE
#define CO_NET_SQPOLL

#include <zmq.h>

#include <latch>

#include "co_net/co_net.hpp"

using namespace net;
using namespace net::tcp;
using namespace net::async;

Task<> remote_lat(int message_size, int roundtrip_count, std::string ip) {
    auto conn = co_await TcpConnection::connect_to(ip::make_addr_v4(ip, 20589));

    Dump(), conn.fd();

    std::vector<char> msg(message_size, 'H');
    std::vector<char> recvd(message_size);

    void* watch;
    watch = zmq_stopwatch_start();

    for (size_t i{}; i != roundtrip_count; ++i) {
        auto written = co_await conn.write_all(msg);

        auto read = co_await conn.sync_read_some(message_size);

        if (read != message_size) [[unlikely]] {
            Dump(), read, "?";
            std::exit(1);
        }
    }

    conn.shutdown_write();

    auto elapsed = zmq_stopwatch_stop(watch);

    auto latency = (double)elapsed / (roundtrip_count * 2);

    printf("message size: %d [B]\n", (int)message_size);
    printf("roundtrip count: %d\n", (int)roundtrip_count);
    printf("average latency: %.3f [us]\n", (double)latency);
    printf("Ctrl + C to quit\n");

    for (;;) {}

    co_return;
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::printf("Usage: ./remote_lat <message size> <round_trip count> <server ip> \n");
        return 1;
    }
    std::string ip = argc <= 3 ? "localhost" : argv[3];
    return net::context::block_on(&remote_lat, std::atoi(argv[1]), std::atoi(argv[2]), ip);
}