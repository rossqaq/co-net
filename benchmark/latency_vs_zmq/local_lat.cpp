#define CO_NET_NO_WORKER_MODE
#define CO_NET_SQPOLL

#include <zmq.h>

#include "co_net/co_net.hpp"

using namespace net;
using namespace net::tcp;
using namespace net::async;

Task<> local_lat(int sz, int cnt) {
    auto listener = co_await TcpListener::listen_on(ip::make_addr_v4("localhost", 20589));

    Dump(), listener.listen_fd();

    auto conn = co_await listener.accept();

    Dump(), "Connection:", conn.fd();

    int bid{};
    for (size_t i{}; i != cnt; ++i) {
        auto n = co_await conn.sync_ring_buf_receive(sz);
        if (n != sz) [[unlikely]] {
            Dump(), n;
            std::exit(1);
        }
        auto sended = co_await conn.send_last_receive();
    }

    conn.shutdown_read();

    for (;;) {}
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::printf("Usage: ./local_lat <message size> <round_trip count>\n");
        return 1;
    }
    return net::context::block_on(&local_lat, std::atoi(argv[1]), std::atoi(argv[2]));
}