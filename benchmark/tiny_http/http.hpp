#pragma once

#include <array>
#include <vector>

#include "co_net/co_net.hpp"

using namespace net;
using namespace net::io;
using namespace net::tcp;
using namespace net::async;

#define SERVER_NAME "Server: co-net server/0.0.1\r\n"

const char* http_content = "<!DOCTYPE html>"
                           "<html>"
                           "<head>"
                           "<meta charset=\"UTF-8\">"
                           "</head>"
                           "<body>"
                           "<h1>"
                           "Hello, co-net!"
                           "</h1>"
                           "</body>"
                           "</html>";

Task<int> response(TcpConnection& conn) {
    int header{};
    std::array<char, 1024> buf;

    sprintf(buf.data(),
            "HTTP/1.1 200 OK\r\n"
            "Content-Type: text/html\n"
            "Cache-Control: no-cache\n"
            "Content-Length: %d\n"
            "Access-Control-Allow-Origin: *\n\n"
            "%s\n",
            strlen(http_content),
            http_content);

    auto response = co_await conn.write_some(buf, strlen(buf.data()));

    co_return response;
}

Task<int> response_unimplemented(TcpConnection& conn) {
    int len{};
    std::array<char, 2048> buf;

    sprintf(buf.data(), "HTTP/1.1 501 Method Not Implemented\r\n");
    auto status = co_await conn.write_some(buf, strlen(buf.data()));
    len += status;

    sprintf(buf.data(), SERVER_NAME);
    auto server = co_await conn.write_some(buf, strlen(buf.data()));
    len += server;

    sprintf(buf.data(), "Content-Type: text/html;charset=utf-8\r\n");
    auto content = co_await conn.write_some(buf, strlen(buf.data()));
    len += content;

    sprintf(buf.data(), "\r\n");
    auto crlf = co_await conn.write_some(buf, strlen(buf.data()));
    len += crlf;

    sprintf(buf.data(), "<HTML><HEAD><TITLE>Method Not Implemented\r\n");
    auto title = co_await conn.write_some(buf, strlen(buf.data()));
    len += title;

    sprintf(buf.data(), "</TITLE></HEAD>\r\n");
    auto title_end = co_await conn.write_some(buf, strlen(buf.data()));
    len += title_end;

    sprintf(buf.data(), "<BODY><P>HTTP request method not supported.\r\n");
    auto body = co_await conn.write_some(buf, strlen(buf.data()));
    len += body;

    sprintf(buf.data(), "</BODY></HTML>\r\n");
    auto body_end = co_await conn.write_some(buf, strlen(buf.data()));
    len += body_end;

    co_return len;
}

bool reqeust_get(std::span<char> buf) {
    return true;
}