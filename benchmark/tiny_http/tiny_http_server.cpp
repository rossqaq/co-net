#define CO_NET_NO_WORKER_MODE
#define CO_NET_SQPOLL

#include "co_net/co_net.hpp"
#include "http.hpp"

using namespace net;
using namespace net::io;
using namespace net::tcp;
using namespace net::async;

Task<> http_process(TcpConnection conn) {
    auto nread = co_await conn.ring_buf_receive();
    auto request = conn.move_out_received();
    if (reqeust_get(request)) [[likely]] {
        auto len = co_await response(conn);
    } else {
        auto unimplmented = co_await response_unimplemented(conn);
    }

    co_return;
}

Task<> server() {
    auto listener = co_await TcpListener::listen_on(ip::make_addr_v4("localhost", 20589));
    for (;;) {
        auto conn_list = co_await listener.multishot_accept_with_no_dispatch();
        for (auto& conn : conn_list) {
            Dump(), conn.fd();
            net::context::co_spawn(&http_process, std::move(conn));
        }
        co_await PendingAwaiter{};
    }
}

int main() {
    return net::context::block_on(&server);
}