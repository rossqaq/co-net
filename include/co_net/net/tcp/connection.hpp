#pragma once

#include <liburing.h>

#include <functional>
#include <span>
#include <string_view>
#include <utility>
#include <vector>

#include "co_net/async/task.hpp"
#include "co_net/context/context.hpp"
#include "co_net/io/prep/prep_close.hpp"
#include "co_net/io/prep/prep_connect.hpp"
#include "co_net/io/prep/prep_read.hpp"
#include "co_net/io/prep/prep_recv_in_ring_buf.hpp"
#include "co_net/io/prep/prep_timeout.hpp"
#include "co_net/io/prep/prep_write.hpp"
#include "co_net/net/socket.hpp"
#include "co_net/util/noncopyable.hpp"

namespace net::tcp {

class TcpConnection : public ::tools::Noncopyable {
public:
    explicit TcpConnection(int direct_socket) : socket_(direct_socket), direct_fd_in_sys_ring_(direct_socket) {}

    TcpConnection(TcpConnection&& rhs) :
        socket_(std::exchange(rhs.socket_, -1)),
        direct_fd_in_sys_ring_(std::exchange(rhs.direct_fd_in_sys_ring_, -1)),
        direct_fd_in_this_ring_(std::exchange(rhs.direct_fd_in_this_ring_, -1)),
        send_buffer_(rhs.move_out_sended()),
        receive_buffer_(rhs.move_out_received()),
        ownership_in_sys_ring_(rhs.ownership_in_sys_ring_) {}

    TcpConnection& operator=(TcpConnection&& rhs) {
        socket_ = std::exchange(rhs.socket_, -1);
        direct_fd_in_sys_ring_ = std::exchange(rhs.direct_fd_in_sys_ring_, -1);
        direct_fd_in_this_ring_ = std::exchange(rhs.direct_fd_in_this_ring_, -1);
        send_buffer_ = rhs.move_out_sended();
        receive_buffer_ = rhs.move_out_received();
        ownership_in_sys_ring_ = rhs.ownership_in_sys_ring_;
        return *this;
    }

    ~TcpConnection() {
        if (ownership_in_sys_ring_) {
            sys_ctx::sys_uring_loop->close_direct_socket(socket_);
            direct_fd_in_sys_ring_ = -1;
        } else {
            if (direct_fd_in_this_ring_ > -1) [[likely]] {
                this_ctx::local_uring_loop->close_direct_socket(socket_);
            }
            direct_fd_in_this_ring_ = -1;
        }
        socket_ = -1;
    }

    void release() {
        if (ownership_in_sys_ring_ == false) {
            throw std::logic_error("You cannot release the connection onwership in worker ring loop.");
        }
        direct_fd_in_sys_ring_ = -1;
        ownership_in_sys_ring_ = false;
    }

    void set_subring_socket(int subring_direct_socket) {
        socket_ = subring_direct_socket;
        direct_fd_in_this_ring_ = subring_direct_socket;
        ownership_in_sys_ring_ = false;
    }

    [[nodiscard]]
    int fd() const noexcept {
        return socket_;
    }

    ::net::async::Task<ssize_t> read_some(std::span<char> buf) {
        co_return co_await ::net::io::operation::prep_read(socket_, buf);
    }

    ::net::async::Task<ssize_t> sync_read_some(size_t len) {
        receive_buffer_.resize(len);
        int nread{};
        while (nread < len) {
            auto n =
                co_await ::net::io::operation::prep_read_len(socket_,
                                                             { receive_buffer_.begin() + nread, receive_buffer_.end() },
                                                             len - nread);
            nread += n;
        }
        co_return nread;
    }

    ::net::async::Task<ssize_t> write_some(std::span<char> buf, size_t len) {
        co_return co_await ::net::io::operation::prep_write(socket_, buf, len);
    }

    ::net::async::Task<ssize_t> write_all(std::span<char> buf) {
        co_return co_await ::net::io::operation::prep_write(socket_, buf);
    }

    ::net::async::Task<ssize_t> ring_buf_receive() {
        auto [res, flag] = co_await ::net::io::operation::prep_recv_in_ring_buf(socket_);

        while (res == -ENOBUFS) [[unlikely]] {
            if constexpr (!config::AUTO_EXTEND_WHEN_NOBUFS) {
                co_await sleep();
            } else {
                this_ctx::local_ring_buffer->extend_buffer();
            }

            auto [res_, flag_] = co_await ::net::io::operation::prep_recv_in_ring_buf(socket_);
            res = res_;
            flag = flag_;
        }

        int bid = flag >> IORING_CQE_BUFFER_SHIFT;

        receive_buffer_.clear();

        this_ctx::local_ring_buffer->copy_data(receive_buffer_, bid, res);

        this_ctx::local_ring_buffer->repay(bid);

        co_return res;
    }

    ::net::async::Task<ssize_t> sync_ring_buf_receive(size_t len) {
        int nread{};

        std::vector<int> bids;

        while (nread < len) {
            auto [res, flag] = co_await ::net::io::operation::prep_recv_in_ring_buf(socket_, len - nread);

            while (res == -ENOBUFS) [[unlikely]] {
                if constexpr (!config::AUTO_EXTEND_WHEN_NOBUFS) {
                    co_await sleep();
                } else {
                    this_ctx::local_ring_buffer->extend_buffer();
                }

                auto [res_, flag_] = co_await ::net::io::operation::prep_recv_in_ring_buf(socket_, len - nread);
                res = res_;
                flag = flag_;
            }

            nread += res;

            int bid = flag >> IORING_CQE_BUFFER_SHIFT;

            bids.push_back(bid);
        }

        receive_buffer_.clear();

        this_ctx::local_ring_buffer->copy_data(receive_buffer_, bids, nread);

        this_ctx::local_ring_buffer->repay(bids);

        co_return nread;
    }

    ::net::async::Task<ssize_t> send_last_receive() {
        std::swap(send_buffer_, receive_buffer_);
        co_return co_await write_all(send_buffer_);
    }

    ::net::async::Task<std::span<char>> ring_buf_receive_as_view(int& cache_bid) {
        auto [res, flag] = co_await ::net::io::operation::prep_recv_in_ring_buf(socket_);

        while (res == -ENOBUFS) {
            if constexpr (!config::AUTO_EXTEND_WHEN_NOBUFS) {
                co_await sleep();
            } else {
                this_ctx::local_ring_buffer->extend_buffer();
            }

            auto [res_, flag_] = co_await ::net::io::operation::prep_recv_in_ring_buf(socket_);
            res = res_;
            flag = flag_;
        }

        cache_bid = flag >> IORING_CQE_BUFFER_SHIFT;

        int offset = cache_bid * config::BUFFER_RING_SIZE;

        co_return this_ctx::local_ring_buffer->get_buf_view(cache_bid, res);
    }

    ::net::async::Task<ssize_t> ring_buf_send_bid_cache_view(int bid, std::span<char> buf) {
        auto bytes_write = co_await write_all(buf);

        this_ctx::local_ring_buffer->repay(bid);

        co_return bid;
    }

    void shutdown_write() {
        auto* sqe = this_ctx::local_uring_loop->get_sqe();

        io_uring_prep_shutdown(sqe, socket_, SHUT_WR);

        sqe->flags |= IOSQE_FIXED_FILE | IOSQE_CQE_SKIP_SUCCESS;

        this_ctx::local_uring_loop->submit_all();
        socket_ = -1;
    }

    void shutdown_read() {
        auto* sqe = this_ctx::local_uring_loop->get_sqe();

        io_uring_prep_shutdown(sqe, socket_, SHUT_RD);

        sqe->flags |= IOSQE_FIXED_FILE | IOSQE_CQE_SKIP_SUCCESS;

        this_ctx::local_uring_loop->submit_all();
        socket_ = -1;
    }

    ::net::async::Task<void> sleep(__kernel_timespec tm = config::NOBUFS_SLEEP_FOR) {
        co_await ::net::io::operation::async_sleep_for(tm);
    }

    std::vector<char> move_out_sended() { return std::move(send_buffer_); }

    std::vector<char> copy_sended() const noexcept { return send_buffer_; }

    std::vector<char> move_out_received() { return std::move(receive_buffer_); }

    std::vector<char> copy_received() const noexcept { return receive_buffer_; }

    static ::net::async::Task<TcpConnection> connect_to(::net::ip::SocketAddr addr) {
        auto socket = co_await ::net::create_tcp_direct_socket(addr);

        if (addr.is_ipv4()) {
            co_await ::net::io::operation::prep_connect_to(socket,
                                                           reinterpret_cast<sockaddr*>(&addr.sockaddr_v4()),
                                                           addr.len());
        } else {
            co_await ::net::io::operation::prep_connect_to(socket,
                                                           reinterpret_cast<sockaddr*>(&addr.sockaddr_v6()),
                                                           addr.len());
        }

        co_return TcpConnection{ socket };
    }

private:
    int socket_{ -1 };

    int direct_fd_in_sys_ring_{ -1 };

    int direct_fd_in_this_ring_{ -1 };

    bool ownership_in_sys_ring_{ true };

    std::vector<char> send_buffer_;

    std::vector<char> receive_buffer_;
};

}  // namespace net::tcp

namespace net::io {

void Uring::impl_handle_msg_conn(io_uring_cqe* cqe) {
    auto* task_token = reinterpret_cast<msg::RingMsgConnTaskHelper*>(cqe->user_data);
    auto task = task_token->move_out();
    auto sys_conn_fd = task_token->direct_fd_;
    delete task_token;

    auto connfd = direct_map_[sys_conn_fd];
    direct_map_.erase(sys_conn_fd);

    auto conn = net::tcp::TcpConnection{ -1 };
    conn.set_subring_socket(connfd);
    peer_task_loop_->emplace_back(std::move(task), std::move(conn));

    notify_main_ring_release_fd(sys_conn_fd);
}

#ifndef CO_NET_NO_WORKER_MODE
void Uring::impl_handle_macc_task(io_uring_cqe* cqe) {
    auto* task_token = reinterpret_cast<msg::MultishotAcceptTaskToken*>(cqe->user_data);
    auto func = task_token->move_out();
    delete task_token;

    for (auto fd : awaiting_direct_fd_) {
        auto task = std::invoke(func, net::tcp::TcpConnection{ fd });
        peer_task_loop_->enqueue(std::move(task));
    }
}
#endif

}  // namespace net::io