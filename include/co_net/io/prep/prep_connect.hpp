#pragma once

#include <liburing.h>

#include "co_net/context/context.hpp"
#include "co_net/io/prep/uring_awaiter.hpp"

namespace net::io::operation {

class ConnectAwaiter : public net::io::UringAwaiter<ConnectAwaiter> {
public:
    using UringAwaiter = net::io::UringAwaiter<ConnectAwaiter>;

    template <typename F>
        requires std::is_invocable_v<F, io_uring_sqe*>
    ConnectAwaiter(io::Uring* ring, F&& func) : UringAwaiter(ring, std::forward<F>(func)) {
        token_.op_ = Op::Connect;
        sqe_->flags |= IOSQE_FIXED_FILE;
    }

public:
    template <typename Promise>
    void await_suspend(std::coroutine_handle<Promise> caller) {
        caller.promise().this_chain_task_->set_awaiting_handle(caller);
        token_.chain_task_ = caller.promise().this_chain_task_;
        token_.chain_task_->set_pending(true);
        ::this_ctx::local_uring_loop->submit_all();
        return;
    }
};

inline net::async::Task<int> prep_connect_to(int socket, sockaddr* addr, socklen_t len) {
    auto [res, flag] = co_await ConnectAwaiter{ ::this_ctx::local_uring_loop, [&](io_uring_sqe* sqe) {
                                                   io_uring_prep_connect(sqe, socket, addr, len);
                                               } };

    if (res < 0) [[unlikely]] {
#ifdef CO_NET_USE_EXCEPTION
        throw std::runtime_error("io_uring prep connect failed.");
#else
        Dump(), strerror(-res);
#endif
    }

    co_return res;
}

}  // namespace net::io::operation